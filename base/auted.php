<?php
/**
 * Déclarations relatives à la base de données
 *
 * @plugin     Autorisations étendues
 * @copyright  2020
 * @author     tofulm
 * @licence    GNU/GPL
 * @package    SPIP\Auted\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Déclaration des alias de tables et filtres automatiques de champs
 *
 * @pipeline declarer_tables_interfaces
 * @param array $interfaces
 *     Déclarations d'interface pour le compilateur
 * @return array
 *     Déclarations d'interface pour le compilateur
 */
function auted_declarer_tables_interfaces($interfaces) {

	$interfaces['table_des_tables']['aut_modeles'] = 'aut_modeles';

	return $interfaces;
}


/**
 * Déclaration des objets éditoriaux
 *
 * @pipeline declarer_tables_objets_sql
 * @param array $tables
 *     Description des tables
 * @return array
 *     Description complétée des tables
 */
function auted_declarer_tables_objets_sql($tables) {

	$tables['spip_aut_modeles'] = array(
		'type'                => 'aut_modele',
		'principale'          => 'oui',
		'table_objet_surnoms' => array('autmodele'),
		'field'=> array(
			'id_aut_modele' => 'bigint(21) NOT NULL',
			'nom'           => 'varchar(55) NOT NULL DEFAULT ""',
			'aut_modele'    => 'text NOT NULL DEFAULT ""',
			'statut'        => 'varchar(20)  DEFAULT "0" NOT NULL',
			'maj'           => 'TIMESTAMP'
		),
		'key' => array(
			'PRIMARY KEY'        => 'id_aut_modele',
			'KEY statut'         => 'statut',
		),
		'titre' => 'nom AS titre, "" AS lang',
		'champs_editables'  => array('nom', 'aut_modele', 'modifie'),
		'statut_textes_instituer' => array(
			'prepa'    => 'texte_statut_en_cours_redaction',
			'publie'   => 'texte_statut_publie',
			'refuse'   => 'texte_statut_refuse',
			'poubelle' => 'texte_statut_poubelle',
		),
		'statut'=> array(
			array(
				'champ'     => 'statut',
				'publie'    => 'publie',
				'previsu'   => 'publie,prepa',
				'post_date' => 'date',
				'exception' => array('statut','tout')
			)
		),
		'texte_changer_statut' => 'aut_mod:texte_changer_statut_aut_mod',


	);

	return $tables;
}


/**
 * Ajouter des champs aux tables auteurs et articles et rubrique
 * @param array $tables_principales
 * @return array
 */

function auted_declarer_tables_principales($tables_principales){

	$tables_principales['spip_auteurs']['field']['autorisations'] = "text NOT NULL";
	$tables_principales['spip_auteurs']['field']['aut_nom']       = "varchar(55) NOT NULL";
	$tables_principales['spip_auteurs']['field']['boss']          = "varchar(20) NOT NULL";

	return $tables_principales;
}

# Autorisations étendues

Plugins de développement qui permet d'étendre les autorisations de SPIP.
Il est compatible avec le plugin chosen / select2


## Configuration
1. Le plugin a  besoin d'une constante listant les autorisations à ajouter (dans xxx_options.php)
```php
define('_AUTH_MODELE', [
	'produit' => [
		'label' => 'Produit / Stage',
		'type'  => 'produit',
		'faire' => ['voir','creer', 'modifier', 'supprimer']
	],
	'guide' => [
		'label' => 'Guide',
		'type'  => 'guide',
		'faire' => ['voir', 'modifier']
	],
	'client' => [
		'label' => 'Client',
		'type'  => 'client',
		'faire' => ['voir', 'modifier']
	]
]);
```

2. On peut ajouter des criteres de sélection des auteurs qui s'afficheront dans ce formulaire via le pipeline formulaire_charger. ex :
```php
function prefix_formulaire_charger($flux){
	if ($flux['args']['form'] === 'editer_autorisations'){
		$flux['data']['pgp'] = ["guide","client"];
		$flux['data']['webmestre'] = "non";
	}
	return $flux;
}
```

## Formulaire de gestion des autorisations
1. Appeler le formulaire d'édition des autorisations, dans une page de votre choix. ex :
```html
[(#AUTORISER{modifier,autmodele}|sinon_interdire_acces)]
<div class="inner">
	<header>
		<h1 class="txtcenter">Gestion des autorisations</h1>
	</header>
	<div class="">
		[(#FORMULAIRE_EDITER_AUTORISATIONS{#ENV{id_auteur}})]
	</div>
</div>
```
![formulaire_autorisation](./doc/formulaire_autorisations.png "Formulaire_d'autorisations")

2. Si un auteur un dans son champ **boss = oui** => il aura tous les droits (un logo s'affiche devant son nom) : ex Emilie
3. Devant le nom d'un auteur, une petite icone verte (modèle) permet de sélectionner un modèle d'autorisation.
4. Quand un modèle d'autorisation est sélectionné, ex: pour Frédéric -> modèle Secrétaire, il récupère les autorisations définies dans le modèle **Secrétaire** et on ne peut donc plus modifier ses autorisations. Dans ce cas, on peut soit changer de modèle (icone modèle noire) soit dissocier le modèle (cadenas ouvert rouge).
5. Ces autorisations sont stockées en json dans le champ **autorisations** de la table spip_auteurs, on stocke aussi le nom du modèle le cas échant dans le champ **aut_nom**

## Gestion des modèles d'autorisations
> On peut créer des modèles d'autorisations

![formulaire_modeles_autorisations](./doc/formulaire_modeles_autorisations.png "Formulaire_modèles_d'autorisations")


## Utilisation des autorisations
1. Dans le fichier prefix_autorisations.php ajouter en haut du fichier :
```php
include_spip('inc/tester_autorisation');
```
2. Puis ajouter les fonctions d'autorisation, par exemple, pour le type guide, nous avons : voir et modifier
```php
function autoriser_guide_voir_dist($faire, $type, $id, $qui, $opt) {
	return tester_autorisation($faire, $type, $id, $qui);
}
function autoriser_guide_modifier_dist($faire, $type, $id, $qui, $opt) {
	return tester_autorisation($faire, $type, $id, $qui);
}
// comme nos guides sont des auteurs, pour utiliser les crayons, nous surchargeons la fonction de SPIP
function autoriser_auteur_modifier($faire, $type, $id, $qui, $opt) {
	return tester_autorisation($faire, "guide", $id, $qui);
}
```

3. Cette fonction : `tester_autorisation()` renvoie toujours **true** pour les **webmestres** et pour les auteurs avec le champ **boss=oui**.
Elle ajoute un pipeline : `super_autorisation` qui permet de modifier le retour.
Exemple, renvoyer toujours true pour les boss définis dans le champ boss=oui, de l'auteur qui demande l'autorisation
```php
function prefixe_super_autorisation($qui){
	if ($qui['boss'] === 'oui') {
		return true;
	}
	return false;
}
```
> On peut exeptionnellement interdire le passage dans ce pipeline  ou le test $qui['boss'] === 'oui' en appelant notre fonction `tester_autorisation()` avec false en 4e arg
```php
tester_autorisation($faire, $type, $id, $qui, false);
```

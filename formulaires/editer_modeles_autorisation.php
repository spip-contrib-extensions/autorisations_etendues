<?php
if (!defined('_ECRIRE_INC_VERSION')) return;

defined('_AUTH_MODELE') || define('_AUTH_MODELE', []);
function formulaires_editer_modeles_autorisation_charger_dist($redirect){
	$valeurs = [];

	$valeurs['auth_modele'] = (defined('_AUTH_MODELE') and count(_AUTH_MODELE)) ? _AUTH_MODELE : $GLOBALS['_AUTH_MODELE'];

	return $valeurs;
}

function formulaires_editer_modeles_autorisation_verifier_dist($redirect){
	$erreurs = array();
	return $erreurs;
}

function formulaires_editer_modeles_autorisation_traiter_dist($redirect){
	include_spip('inc/session');

	$retour = array();

	if (_request('ajouter_modele') === "add") {
		sql_insertq('spip_aut_modeles', ['statut' => 'publie']);
		if ($redirect) {
			$retour['redirect'] = $redirect;
		}
		return $retour;
	}

	$Tid_aut_modele = _request('Tid_aut_modele');

	foreach ($Tid_aut_modele as $id) {
		$Tauths     = _request('id-' . $id);
		$Tautorisations = [];

		if (!empty($Tauths)) {
			foreach ($Tauths as $valeur) {
				$Tautorisations[key($valeur)][]= $valeur[key($valeur)];
			}
		}

		$set = [
			'aut_modele' => json_encode($Tautorisations),
			'nom'        => _request('Tnom')[$id][0]
		];

		sql_updateq('spip_aut_modeles', $set, 'id_aut_modele='.intval($id));

		if ($set['nom'] === _request('Tnom_modele')[$id][0]) {
			$set_auteur = [
				'autorisations' => json_encode($Tautorisations),
			];
			sql_updateq('spip_auteurs', $set_auteur, 'aut_nom='.sql_quote($set['nom']));
			// on recalcul la session
			$Tauteurs = sql_allfetsel('*', 'spip_auteurs', 'aut_nom='.sql_quote($set['nom']));
			foreach ($Tauteurs as $auteur) {
				actualiser_sessions($auteur);
			}
		}


		$retour['message_ok'] = _T("auted:enregistrement_valide");
	}
	include_spip('inc/invalideur');
	suivre_invalideur("id='id_aut_modele/$id'");


	if ($redirect) {
		$retour['redirect'] = $redirect;
	}

	return $retour;
}

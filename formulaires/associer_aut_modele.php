<?php
if (!defined('_ECRIRE_INC_VERSION')) return;

include_spip('action/editer_autmod');

function formulaires_associer_aut_modele_charger_dist($id_auteur, $aut_nom = 0, $redirect = ''){
	$id_aut_modele = 0;
	if ($aut_nom) {
		$id_aut_modele = sql_getfetsel('id_aut_modele', 'spip_aut_modeles', 'nom='.sql_quote($aut_nom));
	}

	$valeurs = array(
		"id_auteur"     => $id_auteur,
		"id_aut_modele" => intval($id_aut_modele)
	);

	return $valeurs;
}

function formulaires_associer_aut_modele_verifier_dist($id_auteur, $aut_nom = 0, $redirect = ''){
	$erreurs = [];
	return $erreurs;
}

function formulaires_associer_aut_modele_traiter_dist($id_auteur, $aut_nom = 0, $redirect = ''){
	$retour = [];

	$id_aut_modele = _request('id_aut_modele');
	if (intval($id_aut_modele) and intval($id_auteur)) {
		auteur_associer_autmod($id_auteur, $id_aut_modele);
	} else {
		$retour['message_erreur'] = _T('auted:enregistrement_impossible');
	}

	if ($redirect) {
		$retour['redirect'] = $redirect;
	}

	return $retour;
}

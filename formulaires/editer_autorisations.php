<?php
if (!defined('_ECRIRE_INC_VERSION')) return;

defined('_AUTH_MODELE') || define('_AUTH_MODELE', []);
function formulaires_editer_autorisations_charger_dist($id_auteur=0, $redirect=''){
	$id_auteur = (int) $id_auteur;

	if ($id_auteur) {
		$valeurs['id_auteur'] = $id_auteur;
	} else {
		$where = [ sql_in('statut', ['5poubelle', 'contact'], 'NOT') ];
		$where = pipeline('auted_where_select_id_auteur', [
			'args' => ['id_auteur' => $id_auteur],
			'data' => $where
		]);
		$tId_auteur = sql_allfetsel('id_auteur', 'spip_auteurs', $where);
		$valeurs['id_auteur'] = array_column($tId_auteur, 'id_auteur');
	}

	$valeurs['auth_modele'] = (defined('_AUTH_MODELE') and count(_AUTH_MODELE)) ? _AUTH_MODELE : $GLOBALS['_AUTH_MODELE'];

	return $valeurs;
}

function formulaires_editer_autorisations_verifier_dist($id_auteur=0, $redirect=''){
	$erreurs = array();
	return $erreurs;
}

function formulaires_editer_autorisations_traiter_dist($id_auteur=0, $redirect=''){
	include_spip('inc/session');

	$retour = array();

	$Tdisabled  = _request('Tdisabled');
	$Tid_auteur = _request('Tid_auteur');

	$id_auteur_dissocier = _request('dissocier_modele');
	if (intval($id_auteur_dissocier)) {
		include_spip('action/editer_autmod');
		auteur_dissocier_autmod($id_auteur_dissocier);
	}

	foreach ($Tid_auteur as $id) {
		$Tautorisations = [];


		if (is_array($Tdisabled) and in_array($id, $Tdisabled)) {
			continue;
		}

		$Tauths = _request('id-' . $id);
		if (!empty($Tauths)) {
			foreach ($Tauths as $valeur) {
				$Tautorisations[key($valeur)][]= $valeur[key($valeur)];
			}
		}
		$set = ['autorisations' => json_encode($Tautorisations)];
		sql_updateq('spip_auteurs', $set, 'id_auteur='.intval($id));

		// on recalcul la session
		$auteur = sql_fetsel('*', 'spip_auteurs','id_auteur='.intval($id));
		actualiser_sessions($auteur);

		$retour['message_ok'] = _T("auted:enregistrement_valide");
	}

	include_spip('inc/invalideur');
	suivre_invalideur("id='id_auteur/$id'");


	if ($redirect) {
		$retour['redirect'] = $redirect;
	}

	return $retour;
}

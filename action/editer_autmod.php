<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function action_editer_autmod_dist() {
	return false;
}

function auteur_associer_autmod($id_auteur, $id_aut_modele) {
	$auts = sql_fetsel('nom, aut_modele','spip_aut_modeles', 'id_aut_modele='.intval($id_aut_modele));
	if (!empty($auts)) {
		include_spip('inc/session');

		$set = [
			'autorisations' => $auts['aut_modele'],
			'aut_nom'       => $auts['nom']
		];
		sql_updateq('spip_auteurs', $set, 'id_auteur='.intval($id_auteur));
		$retour['message_ok'] = "bravo";

		$auteur = sql_fetsel('*', 'spip_auteurs','id_auteur='.intval($id_auteur));
		actualiser_sessions($auteur);

		include_spip('inc/invalideur');
		suivre_invalideur("id='id_auteur/$id_auteur'");
	}
}

function auteur_dissocier_autmod($id_auteur) {
	include_spip('inc/session');
	sql_updateq('spip_auteurs',['aut_nom' => ''], 'id_auteur='.intval($id_auteur));

	$auteur = sql_fetsel('*', 'spip_auteurs','id_auteur='.intval($id_auteur));

	actualiser_sessions($auteur);

	include_spip('inc/invalideur');
	suivre_invalideur("id='id_auteur/$id_auteur'");
}

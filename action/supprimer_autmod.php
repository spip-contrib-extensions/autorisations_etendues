<?php
if (!defined('_ECRIRE_INC_VERSION')) return;

function action_supprimer_autmod_dist(){

	include_spip('inc/autoriser');
	if (! autoriser("supprimer","autmodele")) {
		return false;
	}


	$securiser_action = charger_fonction('securiser_action', 'inc');
	$arg = $securiser_action();

	$id_aut_modele = intval($arg);

	if ($id_aut_modele) {
		sql_delete('spip_aut_modeles', 'id_aut_modele='.$id_aut_modele);

		// Cache
		include_spip('inc/invalideur');
		suivre_invalideur("id='id_aut_modele/$id_aut_modele'");
	}
}

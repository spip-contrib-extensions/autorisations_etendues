<?php
/**
 * Définit les autorisations du plugin Autorisations étendues
 *
 * @plugin     Autorisations étendues
 * @copyright  2020
 * @author     tofulm
 * @licence    GNU/GPL
 * @package    SPIP\Auted\Autorisations
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Fonction d'appel pour le pipeline
 * @pipeline autoriser */
function auted_autoriser() {
}


// -----------------
// BOSS
function autoriser_boss_dist($faire, $type, $id, $qui, $opt) {
	if (autoriser('webmestre', null, null, $qui)) {
		return true;
	}
	if (isset($qui['boss']) and $qui['boss'] === 'oui') {
		return true;
	}
	return false;
}

/*
 * Gestion des autorisations
 */
function autoriser_auted_modifier_dist($faire, $type, $id, $qui, $opt) {
	return (autoriser('webmestre', '', '', $qui) or (isset($qui['boss']) and $qui['boss'] === "oui"));
}


// -----------------
// Objet aut_modeles

/**
 * Autorisation de créer (autmodele)
 *
**/
function autoriser_autmodele_associer_dist($faire, $type, $id, $qui, $opt) {
	return (autoriser('webmestre', '', '', $qui) or (isset($qui['boss']) and $qui['boss'] === "oui"));
}

/**
 * Autorisation de modifier (autmodele)
 *
**/
function autoriser_autmodele_modifier_dist($faire, $type, $id, $qui, $opt) {
	return (autoriser('webmestre', '', '', $qui) or (isset($qui['boss']) and $qui['boss'] === "oui"));
}

/**
 * Autorisation de supprimer (autmodele)
 *
**/
function autoriser_autmodele_supprimer_dist($faire, $type, $id, $qui, $opt) {
	return (autoriser('webmestre', '', '', $qui) or (isset($qui['boss']) and $qui['boss'] === "oui"));
}

<?php
if (!defined('_ECRIRE_INC_VERSION')){
	return;
}


function tester_autorisation($faire, $type,  $id = 0, $qui = [], $secret = true, $opt = []) {
	include_spip('inc/autoriser');
	if (autoriser('webmestre', null, null, $qui)) {
		return true;
	}

	if ($secret) {
		$p = pipeline('super_autorisation',[
			'args' => [
				'faire' => $faire,
				'type'  => $type,
				'id'    => $id,
				'qui'   => $qui,
				'opt'   => $opt
			], 'data' => null
		]);

		if (!empty($p) || is_bool($p)) {
			return $p;
		}

		if (isset($qui['boss']) and $qui['boss'] === "oui") {
			return true;
		}
	}

	$Tauts = [];
	if (isset($qui['autorisations']) and !empty($qui['autorisations'])) {
		$Tauts = json_decode($qui['autorisations'],true);
	}

	if (array_key_exists($type, $Tauts)) {
		if (in_array($faire, $Tauts[$type])) {
			// si on demande pour un $id specifique
			if ( intval($id)) {
				$test_auteurs_objet = charger_fonction('test_auteurs_objet','');
				$cond = ['qui' => $qui, 'faire' => $faire];
				if (is_array($opt) and count($opt)) {
					$cond = array_merge($cond, $opt);
				}
				if ($test_auteurs_objet($type, $id, $cond)) {
					return true;
				} else {
					return false;
				}
			}
			return true;
		}
	}
	return false;
}

function test_auteurs_objet_dist($objet, $id_objet, $cond){
	if (isset($cond['qui']['id_auteur'])) {
		return auteurs_objet($objet, $id_objet, 'id_auteur=' . $cond['qui']['id_auteur']);
	}
	return false;
}

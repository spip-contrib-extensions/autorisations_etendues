<?php
/**
 * Utilisations de pipelines par Autorisations étendues
 *
 * @plugin     Autorisations étendues
 * @copyright  2020
 * @author     tofulm
 * @licence    GNU/GPL
 * @package    SPIP\Auted\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Charger des styles CSS
 *
 * @pipeline insert_head_css
 * @param string $flux Code html des styles CSS à charger
 * @return string Code html complété
 **/
function auted_insert_head_css($flux){
	$flux .= '<link rel="stylesheet" href="' . timestamp(find_in_path('css/auted.css')) . '" type="text/css" />' . "\n";
	return $flux;
}

/**
 * Optimiser la base de données
 *
 * Supprime les objets à la poubelle.
 *
 * @pipeline optimiser_base_disparus
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
 */
function auted_optimiser_base_disparus($flux) {

	sql_delete('spip_aut_modeles', "statut='poubelle' AND maj < " . sql_quote(trim($flux['args']['date'], "'")));

	return $flux;
}
